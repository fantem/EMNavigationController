//
//  EMultiViewController.m
//  EMultiNavigation
//
//  Created by EasonWang on 13-11-23.
//  Copyright (c) 2013年 EasonWang. All rights reserved.
//




#define NavViewFrame CGRectMake(0, -20, 320, 64)
#define titleLabelFrame CGRectMake(60, 20, 200, 44)
#define backGroundImageFrame CGRectMake(0, 0, 320, 67)
#define leftButtonFrame CGRectMake(10,27,50,30)
#define rightButtonFrame CGRectMake(260,27,50,30)

#define defaultBackGroundColro [UIColor grayColor]

#define TitleFont 18


#import "EMViewController.h"


@interface EMViewController ()

@end

@implementation EMViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
//    self = [super initWithNibName:(IPHONE5?nibNameOrNil:[NSString stringWithFormat:@"%@_4",nibNameOrNil]) bundle:nibBundleOrNil];
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    setViewBounds(self);
    
    // 初始化活动指示器(LoadingView)
    self.loadingView = [[LoadingView alloc]initWithDefault];
    
    _NavView = [[UIView alloc]initWithFrame:NavViewFrame];
    _navBackGroundImageView = [[UIImageView alloc]initWithFrame:backGroundImageFrame];
    [_NavView addSubview:_navBackGroundImageView];
    _titleLabel = [[UILabel alloc]initWithFrame:titleLabelFrame];
    _titleLabel.backgroundColor = [UIColor clearColor];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [_titleLabel setFont:[UIFont systemFontOfSize:TitleFont]];
    [_NavView addSubview:_titleLabel];
    // 添加返回按钮
    _backButton = [[UIButton alloc]initWithFrame:leftButtonFrame];
//    [_backButton setTitle:@"back" forState:UIControlStateNormal];
    UIImage *backImage = [UIImage imageNamed:@"backicon.png"];
    [_backButton setBackgroundImage:backImage forState:UIControlStateNormal];
    [_backButton addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
    if (NavigationController.viewControllers.count<=1) {
        _backButton.hidden = YES;
    }
    [_NavView addSubview:_backButton];
    [self.view addSubview:_NavView];
    
    
    // 设置navigationBar背景颜色
    [_NavView setBackgroundColor:[UIColor grayColor]];
    // 设置NavigationBar背景图片
    [_navBackGroundImageView setBackgroundColor:[UIColor clearColor]];
    UIImage *image = [UIImage imageNamed:@"NavBG.png"];
    self.navBackGroundImage = image;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - the property setter method
// backGroundColor
-(void)setNavBackGroundColor:(UIColor *)navBackGroundColor
{
    [_NavView setBackgroundColor:((navBackGroundColor)?navBackGroundColor:defaultBackGroundColro)];
}
// backGroundImage
-(void)setNavBackGroundImage:(UIImage *)navBackGroundImage
{
    [_navBackGroundImageView setImage:navBackGroundImage];
}

// title
-(void)setNavTitle:(NSString *)navTitle
{
    _navTitle = navTitle;
    _titleLabel.text = _navTitle;
}

// title color
-(void)setNavTitleColor:(UIColor *)navTitleColor
{
    _navTitleColor = navTitleColor;
    [_titleLabel setTextColor:_navTitleColor];
}

// backButton title
-(void)setBackBtnTitle:(NSString *)backBtnTitle
{
    _backBtnTitle = backBtnTitle;
    [_backButton setTitle:_backBtnTitle forState:UIControlStateNormal];
}

// backButton image
-(void)setBackBtnImage:(UIImage *)backBtnImage
{
    _backBtnImage = backBtnImage;
    [_backButton setTitle:nil forState:UIControlStateNormal];
    [_backButton setBackgroundImage:_backBtnImage forState:UIControlStateNormal];
}

#pragma mark - navigationBar items method
// 设置leftItem(title)
-(void)setLeftItemWithTarget:(id)target action:(SEL)action title:(NSString *)title
{
    _backButton.hidden = YES;
    UIButton *leftButton = [[UIButton alloc]initWithFrame:leftButtonFrame];
    [leftButton setTitle:title forState:UIControlStateNormal];
    [leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_NavView addSubview:leftButton];
}

// 设置leftItem(image)
-(void)setLeftItemWithTarget:(id)target action:(SEL)action image:(UIImage *)image
{
    _backButton.hidden = YES;
    UIButton *leftButton = [[UIButton alloc]initWithFrame:leftButtonFrame];
    [leftButton setBackgroundImage:image forState:UIControlStateNormal];
    [leftButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_NavView addSubview:leftButton];
}

// 设置rightItem(title)
-(void)setRightItemWithTarget:(id)target action:(SEL)action title:(NSString *)title
{
    UIButton *rightButton = [[UIButton alloc]initWithFrame:rightButtonFrame];
    [rightButton setTitle:title forState:UIControlStateNormal];
    [rightButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_NavView addSubview:rightButton];
}

// 设置rightItem(image)
-(void)setRightItemWithTarget:(id)target action:(SEL)action image:(UIImage *)image
{
    UIButton *rightButton = [[UIButton alloc]initWithFrame:rightButtonFrame];
    [rightButton setBackgroundImage:image forState:UIControlStateNormal];
    [rightButton addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_NavView addSubview:rightButton];
}


#pragma mark - the backButton method
// go back method
-(void)goBack
{
    [NavigationController popViewControllerAnimated:YES];
}
@end
